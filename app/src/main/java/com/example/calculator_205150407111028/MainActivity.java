package com.example.calculator_205150407111028;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {

    TextView textview;
    Button b1, b2, b3, b4;
    Button b5, b6, b7, b8, b9, b0;
    Button bc, bp, bm, bk, bb, akhir;
    int num, opr;
    Intent intent;
    String operasi;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        textview = (TextView) findViewById(R.id.textView1);
        num = 0; opr = 0; operasi = "";
        b1 = (Button) findViewById(R.id.b1);
        b2 = (Button) findViewById(R.id.b2);
        b3 = (Button) findViewById(R.id.b3);
        b4 = (Button) findViewById(R.id.b4);
        b5 = (Button) findViewById(R.id.b5);
        b6 = (Button) findViewById(R.id.b6);
        b7 = (Button) findViewById(R.id.b7);
        b8 = (Button) findViewById(R.id.b8);
        b9 = (Button) findViewById(R.id.b9);
        bc = (Button) findViewById(R.id.bc);
        b0 = (Button) findViewById(R.id.b0);
        bp = (Button) findViewById(R.id.bp);
        bm = (Button) findViewById(R.id.bm);
        bk = (Button) findViewById(R.id.bk);
        bb = (Button) findViewById(R.id.bb);
        akhir = (Button) findViewById(R.id.akhir);

        b1.setOnClickListener(this);
        b2.setOnClickListener(this);
        b1.setOnClickListener(this);
        b2.setOnClickListener(this);
        b3.setOnClickListener(this);
        b4.setOnClickListener(this);
        b5.setOnClickListener(this);
        b6.setOnClickListener(this);
        b7.setOnClickListener(this);
        b8.setOnClickListener(this);
        b9.setOnClickListener(this);
        bc.setOnClickListener(this);
        b0.setOnClickListener(this);
        bp.setOnClickListener(this);
        bm.setOnClickListener(this);
        bk.setOnClickListener(this);
        bb.setOnClickListener(this);
        akhir.setOnClickListener(this);
        intent = new Intent(this, MainActivity2.class);
    }

    void calculate(){
        int num2 = Integer.parseInt(textview.getText().toString());
        if (opr == 1) {
            num += num2;
        } else if (opr == 2) {
            num -= num2;
        } else if (opr == 3) {
            num *= num2;
        } else if (opr == 4) {
            num /= num2;
        }
    }

    @Override
    public void onClick(View view) {
        boolean cek = textview.getText().toString().equals("0");
        if(view.getId() == R.id.akhir) {
            operasi += textview.getText();
            calculate();
            intent.putExtra("operasi", operasi);
            intent.putExtra("hasil", String.valueOf(num));
            startActivity(intent);

            num = 0; operasi = "";
            textview.setText("0");
        }else if(view.getId() == R.id.bp) {
            operasi += textview.getText() + "+";
            if(num != 0) {
                calculate();
                textview.setText(String.valueOf(num));
            }
            num = Integer.parseInt(textview.getText().toString());
            textview.setText("0");
            opr = 1;
        }else if(view.getId() == R.id.bm) {
            operasi += textview.getText() + "-";
            if(num != 0) {
                calculate();
                textview.setText(String.valueOf(num));
            }
            num = Integer.parseInt(textview.getText().toString());
            textview.setText("0");
            opr = 2;
        }else if(view.getId() == R.id.bk) {
            operasi += textview.getText() + "*";
            if(num != 0) {
                calculate();
                textview.setText(String.valueOf(num));
            }
            num = Integer.parseInt(textview.getText().toString());
            textview.setText("0");
            opr = 3;
        }else if(view.getId() == R.id.bb) {
            operasi += textview.getText() + "/";
            if(num != 0) {
                calculate();
                textview.setText(String.valueOf(num));
            }
            num = Integer.parseInt(textview.getText().toString());
            textview.setText("0");
            opr = 4;
        }else if(view.getId() == R.id.bc) {
            textview.setText("0");
            num = 0;
        }else{
            if(view.getId() == R.id.b1){
                if(cek) textview.setText("1");
                else textview.setText(textview.getText() + "1");
            }else if(view.getId() == R.id.b2){
                if(cek) textview.setText("2");
                else textview.setText(textview.getText() + "2");
            }else if(view.getId() == R.id.b3){
                if(cek) textview.setText("3");
                else textview.setText(textview.getText() + "3");
            }else if(view.getId() == R.id.b4){
                if(cek) textview.setText("4");
                else textview.setText(textview.getText() + "4");
            }else if(view.getId() == R.id.b5){
                if(cek) textview.setText("5");
                else textview.setText(textview.getText() + "5");
            }else if(view.getId() == R.id.b6){
                if(cek) textview.setText("6");
                else textview.setText(textview.getText() + "6");
            }else if(view.getId() == R.id.b7){
                if(cek) textview.setText("7");
                else textview.setText(textview.getText() + "7");
            }else if(view.getId() == R.id.b8){
                if(cek) textview.setText("8");
                else textview.setText(textview.getText() + "8");
            }else if(view.getId() == R.id.b9) {
                if(cek) textview.setText("9");
                else textview.setText(textview.getText() + "9");
            }else if(view.getId() == R.id.b0) {
                textview.setText(textview.getText() + "0");
            }
        }
    }
}

