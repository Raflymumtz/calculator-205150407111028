package com.example.calculator_205150407111028;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.widget.TextView;

public class MainActivity2 extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main2);
        TextView textviewHasil = (TextView) findViewById(R.id.hs1);
        TextView textviewOperasi = (TextView) findViewById(R.id.hs2);
        Intent intent = getIntent();
        String nilai = intent.getStringExtra("hasil");
        String operasi = intent.getStringExtra("operasi");
        textviewOperasi.setText(operasi);
        textviewHasil.setText(nilai);
    }
}